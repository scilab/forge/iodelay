  h1=syslin('c',(%s^2+0.1*%s+7.5)/(%s^4+0.12*%s^3+9*%s^2))
  G1=[iodelay(h1,0.2);iodelay(h1,0.4)]

  scf(1);clf();bode(G1,0.1,5)
  scf(2);clf();nyquist(G1,0.1,5)

  h2=syslin('c',(%s^2+0.1*%s+7.5)/(0.12*%s^3+9*%s^2+1));
  G2=[syslin('c',(%s^2+0.1*%s+7.5)/(0.12*%s^3+9*%s^2+1))
      syslin('c',(%s^2+0.1*%s+7.5)/(0.12*%s^3+9*%s^2+2*%s+1))];
  G=G1.*G2
  scf(3);clf();bode(G,0.1,5)
  
  G(1)=G1(1);
  scf(4);clf();bode(G,0.1,5)

  
