iodelay-0.4 for Scilab 5.3 or greater

This is a prototype for handling continuous time transfer function
with input/output delay in Scilab. 


iodelay can be used to create a continuous time transfer function with delay.
Then most operations on regular transfer function can be applied.

This library contains an overloaded definition of bode, black, 
nyquist and repfreq function for this new data type



If you find bugs please send an email to serge.steer@laposte.net
