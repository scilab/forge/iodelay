mode(-1)
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
mode(1)
s=poly(0,'s');
//Create a regular transfer function
h=syslin('c',(s^2+2*0.9*10*s+100)/(s^2+2*0.3*10.1*s+102.01))
h1=h*syslin('c',(s^2+2*0.1*15.1*s+228.01)/(s^2+2*0.9*15*s+225))
//Add a 0.01s input/output delay
hd1=iodelay(h1,0.02) 
scf(1000);clf()
subplot(211)
bode([h1;hd1],0.01,100)
a=gca();a.title.text='Bode plot';
subplot(223)
black([h1;hd1],-0.01,100)
a=gca();a.title.text='Black plot';
subplot(224)
nyquist([h1;hd1],-100,100)
a=gca();a.title.text='Nyquist plot';


