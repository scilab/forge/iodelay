mode(-1)
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
demopath = get_absolute_file_path("iodelay.dem.gateway.sce");

subdemolist = ["Building transfer function with iodelay"             , "iodelay1.sce"   ; ..
               "Frequency analysis"  , "iodelay2.sce"         ]

subdemolist(:,2) = demopath + subdemolist(:,2)
                                                                                                              
