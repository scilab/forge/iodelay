mode(-1)
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
mode(1)
s=poly(0,'s');
//Create a regular transfer function
h=syslin('c',(s^2+2*0.9*10*s+100)/(s^2+2*0.3*10.1*s+102.01))
//Add a 0.01s input/output delay
hd1=iodelay(h,0.01) 
//Create another delayed transfer function with  0.0133s input/output delay
halt()
hd2=iodelay(h+1,0.0133)
halt()

hds=hd1*hd2 //serial connection
hds2=h*hd2 
halt()

hdp= hd1+iodelay(h+1,0.01)//parallel ci=onnection requires identical delays
halt()



hd=[hd1;hd2] //a SiMo transfert function
halt()

hd(1,1) = h  //assignment
halt()

hd(1,1)
halt()

    
