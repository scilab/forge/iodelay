mode(-1);
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
try
 getversion('scilab');
catch
 error(gettext('Scilab 5.0 or more is required.'));  
end;
// ====================================================================
if ~with_module('development_tools') then
  error(msprintf(gettext("%s module not installed."),'development_tools'));
end
// ====================================================================
TOOLBOX_NAME = 'iodelay_toolbox';
TOOLBOX_TITLE = TOOLBOX_NAME;
// ====================================================================
toolbox_dir = get_absolute_file_path('builder.sce');
tbx_builder_macros(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(toolbox_dir);
clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE 
clear tbx_builder_macros tbx_builder_src tbx_builder_gateway tbx_builder_help tbx_build_loader
    
