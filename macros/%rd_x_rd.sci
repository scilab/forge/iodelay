// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function H=%rd_x_rd(H1,H2)
  H=mlist(['rd','H','iodelay'],H1.H.*H2.H,H1.iodelay+ H2.iodelay)
endfunction
