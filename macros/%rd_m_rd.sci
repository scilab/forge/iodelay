// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function H=%rd_m_rd(H1,H2)
  D1=H1.iodelay
  D2=H2.iodelay
  D=D1*D2;
    for k=1;size(D2,2)
    for l=1:size(D1,1)
      d=D1(l,:)+(D2(:,k)')
      if or(d(1)<>d) then 
        error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Delays mismatch.\n"),"*",1,2))
      end
      D(l,k)=d
    end
  end
  H=mlist(['rd','H','iodelay'],H1.H*H2.H,D)
endfunction
