// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function Hd=iodelay(H,d)
  if H.dt<>'c' then 
    error(msprintf(_("%s: Wrong values for input argument #%d: Continuous time system expected.\n"),"iodelay",1)),
  end
  if size(d,'*')==1 then d=d*ones(H.num),end
  if or(size(H.num)<>size(d)) then 
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"),"iodelay",1,2))
  end
  Hd=mlist(['rd','H','iodelay'],H,d)
endfunction
