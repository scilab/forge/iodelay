// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function H=%rd_s_r(H1,H2)
  D1=H1.iodelay
  if or(D1<>0) then 
    error(msprintf(_("%s: wrong value for input argument #%d : Invalid delay.\n"),"-",1))
  end
  H=mlist(['rd','H','iodelay'],H1.H-H2,D1)
endfunction
