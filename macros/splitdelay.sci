// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [n,f]=splitdelay(tau,ts)
  tolint = 1e4*eps;
  dtau = tau/Ts;
  n = floor(dtau+tolint);
  rho = dtau - n;
  rho(rho<tolint) = 0;
endfunction
