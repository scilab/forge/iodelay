// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function txt=%rd_string(H)
  r = H.H
  N = string(r.num)
  D = string(r.den)
  ln = max(matrix(length(N),1,-1),'r')
  ld = max(matrix(length(D),1,-1),'r')
  l = max(ln,ld)
  [m,n] = size(r.num);
  S = emptystr(m,n)

  kz = find(H.iodelay==0);//zero delay entries
  w = 'exp(' + string(-H.iodelay) + '*' + varn(r) + ') × '; w(kz) = '';

  for i=1:m*n
    pw = part(' ',1:length(w(i)))
    N(i) = pw + part(' ',1:(l(i)-ln(i))/2)+N(i)
    D(i) = pw + part(' ',1:(l(i)-ld(i))/2)+D(i)
    S(i) = w(i) + part('-',ones(1,l(i))) 
  end
  txt = emptystr(3*m,n);
  txt(1:3:$,:) = N
  txt(2:3:$,:) = S
  txt(3:3:$,:) = D
endfunction
