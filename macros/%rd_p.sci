// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================


function %rd_p(H)
//used to display rational fraction with complex coefficients
//The real case is hard coded
// Copyright INRIA
  T=string(H)
  l=max(length(T),'r')
  Te=emptystr(size(T,1),1)
  for k=1:size(T,2)
    Te=Te+part(T(:,k),1:l(k)+1)+'  '
  end
  mprintf("%s\n",Te)
endfunction
