// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function %f_e(libr,funname)
  //this function is a workaround for the hardcoded syntax libr.funname which is
  //buggued in scilab5.1
  str=string(libr);
  path=str(1)
  old=size(who('get'),'*');
  load(path+funname+'.bin')
  new=who('get');new=strcat(new(1:(size(new,'*')-old-1)),',')
  execstr('['+new+']=resume('+new+')')
endfunction
