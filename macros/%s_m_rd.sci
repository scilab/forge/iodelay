// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function H=%s_m_rd(H1,H2)
  D2=H2.iodelay
  for l=1:size(D2,2)
    if or(D2(1,l)<>D2(:,l)) then 
      error(msprintf(_("%s: wrong value for input argument #%d : Invalid delay.\n"),"*",2))
    end
  end
  H=mlist(['rd','H','iodelay'],H1*H2.H,D2)
endfunction
