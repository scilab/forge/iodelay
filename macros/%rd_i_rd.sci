// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function H=%rd_i_rd(varargin)
  H=varargin($)
  Hfrom=varargin($-1)
  
  Hh= H.H
  Hh.num(varargin(1:$-2))=Hfrom.H.num
  Hh.den(varargin(1:$-2))=Hfrom.H.den
  H.H=Hh
  H.iodelay(varargin(1:$-2))=Hfrom.iodelay
endfunction
