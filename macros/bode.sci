// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function bode(varargin)
  H=varargin(1)

  if typeof(H)=='rd' then
    if type(varargin($))==10 then
      [frq,repf,splitf]=repfreqrd(varargin(1:$-1))
      cacsdlib.bode(frq,repf,varargin($))
    else    
      [frq,repf,splitf]=repfreqrd(varargin(1:$))
      cacsdlib.bode(frq,repf)
    end
  else
   cacsdlib.bode(varargin(:))
  end
endfunction
