// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [frq,bnds,splitf]=calfrqrd(h,fmin,fmax)
//!

  eps=1.d-14   //minimum absolute lower frequency
  k=0.0005;     // Minimum relative prediction error in the nyquist plan 
  epss=0.002   // minimum frequency distance with a singularity
  nptmax=10000  //maximum number of discretisation points
  tol=0.01     // Tolerance for testing pure imaginary numbers 
  ephi_max=0.002 //maximum phase delta between two successive points
  // Check inputs
  // ------------
  if typeof(h)<>'rd'
    error(msprintf(gettext("%s: Wrong type for input argument #%d: delayed transfer function expected.\n"),"calfrq",1))
  end

  [m,n]=size(h);
  
  // Use symmetry to reduce the range
  // --------------------------------
  if fmin<0&fmax>=0 then
    [frq,bnds,splitf]=calfrqrd(h,eps,-fmin)
    ns1=size(splitf,'*')-1;
    nsp=size(frq,'*');
    bnds=[bnds(1),bnds(2)];

    if fmax>eps then
      if fmax==-fmin then
	splitf=[1, (nsp+2)*ones(1,ns1)-splitf($:-1:2),nsp*ones(ns1)+splitf(2:$)];
	bnds=[bnds(1),bnds(2)];
	frq=[-frq($:-1:1),frq]
      else
	[frq2,bnds2,splitf2]=calfrqrd(h,eps,fmax);
	ns2=size(splitf2,'*')-1
	splitf=[1, (nsp+2)*ones(1,ns1)-splitf($:-1:2),nsp*ones(ns2)+splitf2(2:$)];
	bnds=[min(bnds(1),bnds2(1)),max(bnds(2),bnds2(2))];
	frq=[-frq($:-1:1),frq2]
      end
      return
    else
      frq=-frq($:-1:1);
      nsp=size(frq,'*');
      
      splitf=[1, (nsp+2)*ones(1,ns1)-splitf($:-1:2)]
      bnds=bnds;
      return;
    end
  elseif fmin<0&fmax<=0 then
    [frq,bnds,splitf]=calfrqrd(h,-fmax,-fmin)
    ns1=size(splitf,'*')-1;
    frq=-frq($:-1:1);
    nsp=size(frq,'*');
    splitf=[1, (nsp+2)*ones(1,ns1)-splitf($:-1:2)]
    bnds=[bnds(1),bnds(2)];
    return;
  elseif fmin >= fmax then
    error(msprintf(gettext("%s: Wrong value for input arguments #%d and #%d: %s < %s expected.\n"),..
		   "calfrq",2,3,"fmin","fmax"));
  end
  // Compute dicretisation over a given range
  // ----------------------------------------
  splitf=[]
  if fmin==0 then fmin=min(1d-14,fmax/10);end
  //
  [m,n]=size(h)
  
  l10=log(10)

  // Locate singularities to avoid them
  // ----------------------------------
  c=2*%pi;
  //selection function for singularities in the frequency range
  deff('f=%sel(r,fmin,fmax,tol)',['f=[],';
		    'if prod(size(r))==0 then return,end';
		    'f=imag(r(find((abs(real(r))<=tol*abs(r))&(imag(r)>=0))))';
		    'if f<>[] then  f=f(find((f>fmin-tol)&(f<fmax+tol)));end']);


  sing=[];zers=[];
  fmin=c*fmin,fmax=c*fmax;

  for i=1:m
    sing=[sing;%sel(roots(h.H.den(i)),fmin,fmax,tol)];
  end

  pp=gsort(sing',"g","d");npp=size(pp,'*');//'
  // singularities just on the left of the range
  kinf=find(pp<fmin)
  if kinf<>[] then
    fmin=fmin+tol
    pp(kinf)=[]
  end

  // singularities just on the right of the range
  ksup=find(pp>=fmax)
  if ksup<>[] then
    fmax=fmax-tol
    pp(ksup)=[]
  end

  //check for nearly multiple singularities
  if pp<>[] then
    dpp=pp(2:$)-pp(1:$-1)
    keq=find(abs(dpp)<2*epss)
    if keq<>[] then pp(keq)=[],end
  end

  if pp<>[] then
    frqs=[fmin real(matrix([(1-epss)*pp;(1+epss)*pp],2*size(pp,'*'),1)') fmax]
    //'
  else
    frqs=[fmin fmax]
  end
  nfrq=size(frqs,'*');
  
  xt=[];Pas=[]
  for i=1:2:nfrq-1
    w=logspace(log(frqs(i))/log(10),log(frqs(i+1))/log(10),100);
    xt=[xt,w]
    Pas=[Pas w(2)-w(1)]
  end

  // Evaluate bounds on modulus
  //---------------------------
  rf=freqd(h,%i*xt);
  bnds=[min(abs(rf)) max(abs(rf))]
  dmag  = max(0.01,bnds(2)-bnds(1))

  
  //Compute the derivative of the transfert function for predictor
  // ------------------------------------------------------------
  hd=iodelay(derivat(h.H)-h.iodelay.*(h.H),h.iodelay);
  //Extract polynomials and delay to speed up computations
  // ------------------------------------------------------
  numh=h.H.num
  denh=h.H.den
  numhd=hd.H.num
  denhd=hd.H.den
  D=h.iodelay

  // Compute discretization with a step adaptation method
  // ----------------------------------------------------
  frq=[];
  i=1;
  nptr=nptmax; // number of unused discretization points
  l10last=log10(frqs($));
  e_phi=0
  while i<nfrq
    f0=frqs(i);fmax=frqs(i+1);
    while f0==fmax do
      i=i+2;
      f=frqs(i);fmax=frqs(i+1);
    end
    frq=[frq,f0];
    rf0=freqd(numh,denh,D,%i*f0);
    rfd=%i*freqd(numhd,denhd,D,%i*f0)
    pas=Pas(floor(i/2)+1)
    splitf=[splitf size(frq,'*')];

    f=min(f0+pas,fmax);
    good=%f
    while f0<fmax
      //Transfer function value at frequency f
      rfc=freqd(numh,denh,D,%i*f);
      //Linearly predicted transfer function value at frequency f
      rfp=rf0+pas*rfd;
      //Prediction error relative to full shape
      e_phi=abs(atan(imag(rfp),real(rfp))-atan(imag(rfc),real(rfc)))
      e=abs(abs(rfp)-abs(rfc))/dmag;
      
      if (e>k|e_phi>ephi_max) then 
        // compute minimum frequency logarithmic step to ensure a maximum 
        //of nptmax points to discretize
        pasmin=f0*(10^((l10last-log10(f0))/(nptr+1))-1)
        pas=pas/2
        if pas<pasmin then
          pas=pasmin
          frq=[frq,f];nptr=max([1,nptr-1])
          good=%t
        else
          f=min(f0+pas,fmax)
        end
      elseif e<k/2 then
        pas=2*pas
        frq=[frq,f];nptr=max([1,nptr-1])
        good=%t
      else
        frq=[frq,f];nptr=max([1,nptr-1])
        good=%t
      end
      if good then
        f0=f;f=min(f0+pas,fmax)
        rf0=freqd(numh,denh,D,%i*f0);
        rfd=%i*freqd(numhd,denhd,D,%i*f0)
      end
    end
    i=i+2 
  end
  frq( size(frq,'*') )=fmax
  frq=frq/c;
  disp(size(frq))
endfunction

