// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function H=%rd_i_s(varargin)
  num=varargin($);den=ones(num)
  Hfrom=varargin($-1)

  num(varargin(1:$-2))=Hfrom.H.num
  den(varargin(1:$-2))=Hfrom.H.den
  D=zeros(num)
  D(varargin(1:$-2))=Hfrom.iodelay
  H=mlist(['rd','H','iodelay'],rlist(num,den,'c'),D)
endfunction
