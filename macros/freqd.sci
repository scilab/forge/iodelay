// ====================================================================
// Copyright (C) INRIA - Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function  rf=freqd(numh,denh,D,s)
  if typeof(numh)=="rd" then
    s=denh;D=numh.iodelay
    denh=numh.H.den
    numh= numh.H.num
  end
  rf=freq(numh,denh,s);
  for k=1:size(D,'*')
    rf(k,:)=rf(k,:).*exp(-D(k)*s)
  end
endfunction
